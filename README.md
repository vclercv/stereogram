## Usage

A depthmap and a texture are needed to generate an autostereogram ( samples available )

    python stereogram.py samples/depthmaps/depthmap_1.png samples/textures/texture_1.png stereo.png

Perspective can be adjusted with arguments --eyeshift, --distance and --front

    python stereogram.py samples/depthmaps/depthmap_1.png samples/textures/texture_1.png stereo.png --front 0.7 --distance 8 --eyeshift 0.5