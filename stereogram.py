# coding: utf-8
import argparse

import cv2 as cv
import numpy as np


class Renderer:

    def __init__(self, depthmap_path, texture_path, stereo_path,
                 front=0.4, distance=5.0, eyeshift=0.):
        self.depthmap = cv.imread(depthmap_path)
        self.texture = cv.imread(texture_path)

        self.depthmap_height, self.depthmap_width, _ = self.depthmap.shape
        self.texture_height, self.texture_width, _ = self.texture.shape

        self.stereo_path = stereo_path

        if 0 <= front <= 1.0:
            self.front = front
        else:
            raise ValueError("front must be between 0. and 1.")

        if distance > 0:
            self.distance = distance
        else:
            raise ValueError("distance must be >0.")

        if -0.5 <= eyeshift <= 0.5:
            self.eyeshift = eyeshift
        else:
            raise ValueError("eyeshift must be between -0.5 and 0.5")

    def process_image(self):

        stereo_data = np.zeros(shape=self.depthmap.shape,
                               dtype=np.uint8)

        for line in range(0, self.depthmap_height):
            texture_line = line % self.texture_height

            offset_map = self.get_offset_map(line)

            # insert the texture at start of line
            stereo_data[line][0:self.texture_width] = self.texture[texture_line]

            # apply changes from left to right based on the start of the line
            for scan_x in range(0, self.depthmap_width):
                z = scan_x + offset_map[scan_x]
                if 0 <= z < self.depthmap_width:
                    stereo_data[line][scan_x] = stereo_data[line][z]

        cv.imwrite(self.stereo_path, stereo_data)

    def get_offset(self, x, line):
        # compute the offset needed for a pixel
        # based on the grey intensity in the depthmap

        # each channel is uint8 : 255*3 = 765
        # casting to int to avoid overflow in uint8
        depth = 1. - self.front * (int(self.depthmap[line][x][0]) +
                                   int(self.depthmap[line][x][1]) +
                                   int(self.depthmap[line][x][2])) / 765.

        return self.texture_width *\
               (1. + self.distance) / (1. + (self.distance / depth))

    def get_offset_map(self, line):
        # default offset is set to a texture width
        offset_map = [-self.texture_width] * self.depthmap_width

        for x in range(0, self.depthmap_width):
            offset = self.get_offset(x, line)
            # modify the point of application of the offset based on eyeshift
            offset_idx = int(x + offset * (0.5-self.eyeshift)
                             + (self.texture_width * self.eyeshift * 2))
            if 0 <= offset_idx < self.depthmap_width:
                offset_map[offset_idx] = max(int(-offset), -self.texture_width)

        return offset_map


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("depthmap_path", help="path of the depth map")
    parser.add_argument("texture_path", help="path of the texture")
    parser.add_argument("output_path")

    parser.add_argument("--eyeshift", "-e", default=0., type=float,
                        help="eyeshift, from -0.5 to 0.5, 0 for a centered perspective")
    parser.add_argument("--front", "-f", default=0.4, type=float,
                        help="defines depth of the nearest point in the depthmap")
    parser.add_argument("--distance", "-d", default=5., type=float,
                        help="distance to the stereogram")

    # Read arguments from the command line
    args = parser.parse_args()

    renderer = Renderer(args.depthmap_path, args.texture_path, args.output_path,
                        eyeshift=args.eyeshift, distance=args.distance, front=args.front)
    renderer.process_image()
